<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
  'prefix' => 'api/v1'], function() use ($router){
              $router->get('/players', 'PlayerController@index');
              $router->get('/players/{id}', 'PlayerController@show');
              $router->post('/players', 'PlayerController@store');
            //  $router->put('/players/{id}', 'PlayerController@update');
              $router->patch('/players/{id}', 'PlayerController@update');
              $router->delete('/players/{id}', 'PlayerController@destroy');
  });
