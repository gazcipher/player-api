<?php

use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seed initial data
        app('db')->table('players')
          ->insert([
            'name' => 'G Ciph',
            'age' => 20,
            'club' => 'Manchester City',
            'gender' => 'Male',
            'nationality' => 'Eden South'
          ]);

          app('db')->table('players')
            ->insert([
              'name' => 'Ciph Junior',
              'age' => 22,
              'club' => 'Juventus',
              'gender' => 'Male',
              'nationality' => 'Asgard'
            ]);
    }
}
