<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PlayersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }


    //========================================Testing Index Page==========================================
    public function testIndex()
    {
        $this->get('api/v1/players')
        ->seeStatusCode(200)
        ->seeJsonStructure([
            '*' => [
                'id',
                'name',
                'age',
                'nationality',
                'club',
                'gender',
                'created_at',
                'updated_at',
            ]
        ]);
    }



    //===================================End Testing======================================================

    //===================================Test Show i.e Successfully Get a Resource========================
    public function testShowSuccess()
    {
        $this->get('api/v1/players/2')
        ->seeStatusCode(200)
        ->seeJsonStructure([
                'id',
                'name',
                'age',
                'nationality',
                'club',
                'gender',
                'created_at',
                'updated_at',
        ])
        ->seeJsonContains([
            'name' => 'Ciph Junior'
        ]);
    }


    //================================== End Get a resource test==========================================

    //===========================================404 Not Found Test. Invalid Id===========================
    public function testShowIdNotFound()
    {
        $this->get("api/v1/players/2029");

        $this->assertEquals('"Invalid Id"', $this->response->getContent());
    }


    //========================================= End 404 Not Found =======================================


    //================================= Resource creation success test ==================================
    public function testStoreSuccess()
    {
        $data = [
            "name" => "Gabriel Skyhawk",
            "age" => 27,
            "nationality" => "Edem South",
            "club" => "Vanguard",
            "gender" => "Male"
        ];

        $this->post('api/v1/players', $data)
        ->seeStatusCode(201)
        ->seeJsonStructure([
                'id',
                'name',
                'age',
                'nationality',
                'club',
                'gender',
                'created_at',
                'updated_at',
        ])
        ->seeJsonContains([
            'name' => 'Gabriel Skyhawk'
        ]);
    }


    //=================================Ends Resource creation============================================

    //=========================================Update Success Testing====================================
    public function testUpdateShouldBeTrue()
    {
        $data = [
            "name" => "Gabriel Skyhawk Vanguard",
            "age" => 27,
            "nationality" => "Edem South",
            "club" => "Vanguard",
            "gender" => "Male"
        ];
        $this->patch('api/v1/players/6', $data)
        ->seeStatusCode(200)
        ->seeJsonStructure([
                'id',
                'name',
                'age',
                'nationality',
                'club',
                'gender',
                'created_at',
                'updated_at',
        ])
        ->seeJsonContains([
            'name' => 'Gabriel Skyhawk Vanguard'
        ]);
    }



    //======================================Ends update testing==========================================

    //================================= Test Update if Id not found======================================
    public function testUpdateShouldShouldBeTrueIfIdNotFound()
    {
        $data = [
            "name" => "Gabriel Skyhawk Vanguard",
            "age" => 27,
            "nationality" => "Edem South",
            "club" => "Vanguard",
            "gender" => "Male"
        ];
        $this->patch('api/v1/players/22222', $data);
        $this->assertEquals(
            '"Invalid Id"', $this->response->getContent()
        );
    }

    //=======================================End id not found test=======================================



    //============================== Delete Resource test ==============================================
    public function testDestroyShouldBeTrue(Type $var = null)
    {
        $this->delete('api/v1/players/2222')->seeStatusCode(204);
    }


    //============================== Ends Resource Deletion ===========================================


    //=============================== Delete test for Invalid Id=======================================
    public function testDestroyShouldBeTrueIfIdNotFound()
    {
        $this->delete('api/v1/players/2222');

        $this->assertEquals('"Invalid Id"', $this->response->getContent());
    }

    //=============================== End Delete test for invalid id =================================













}
