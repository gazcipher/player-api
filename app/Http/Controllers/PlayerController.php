<?php


namespace App\Http\Controllers;
use App\Models\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    protected $_player;

    //Dependency injection
    public function __construct(Player $player)
    {
      $this->_player = $player;
    }

    public function index()
    {
        $players = $this->_player->all();
        return response()->json($players, 200);
    }

    public function show($id)
    {
        $player = $this->_player->find($id);

        if(empty($player))
        {
          return response()->json('Invalid Id', 400);// bad request. Invalid id
        }

        return response()->json($player, 200); //SUCCESS 200 OK
    }

    public function store(Request $request)
    {
          $this->validate($request, [
            'name' => 'required|string:max:64',
            'age' => 'required|integer',
            'nationality' => 'required|string|max:64',
            'club' => 'required|string|max:64',
            'gender' => 'required|string'
          ]);

        //validate input payload
      /*  $validator = Validator::make($request->all(), [
            'name' => 'required|string:max:64',
            'age' => 'required|integer',
            'nationality' => 'required|string|max:64',
            'club' => 'required|string|max:64',
            'gender' => 'required|string'
          ]);

          if($validator->fails())
          {
              return response()->json($validator, 400);
          }*/



        //HTTP post  - resource creation
        $createPlayer = $this->_player->create([
          'name' => $request->input('name'),
          'age' => $request->input('age'),
          'nationality' => $request->input('nationality'),
          'club' => $request->input('club'),
          'gender' => $request->input('gender'),
        ]);

        return response()->json('Success! created.', 201); //201 created
    }

    public function update(Request $request, $id)
    {
        //validate input payload before update
        $this->validate($request, [
          'name' => 'required|string:max:64',
          'age' => 'required|integer',
          'nationality' => 'required|string|max:64',
          'club' => 'required|string|max:64',
          'gender' => 'required|string'
        ]);

        //get the player to update
        $player = $this->_player->find($id);

        //check if there was error or null
        if(empty($player))
        {
          return response()->json("Invalid Id", 400); //Bad request
        }

        //update player now
        $player->update([
          'name' => $request->input('name'),
          'nationality' => $request->input('nationality'),
          'age' => $request->input('age'),
          'gender' => $request->input('gender'),
          'club' => $request->input('club'),
        ]);

        return response()->json($player, 200); //Success 200 Ok
    }

    public function destroy($id)
    {
        $player = $this->_player->find($id);

        //check if id is empty
        if(empty($player))
        {
          return response()->json("Invalid Id", 400); //bad reequest
        }

        $player->delete();

        return response()->json('Deleted', 204); //204 No content to displayer, deletion success
    }



}

?>
